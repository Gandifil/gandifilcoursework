#include "compressing.h"

// узел дерева Хаффмана
struct Node
{
    struct Node *parent, *left, *right;
    uint64_t count;
    uint8_t value;
};

//структура, кодирующая символ
struct Pair
{
    uint8_t keySize; //кол-во бит в ключе
    uint64_t key; //сам ключ
};

FILE *fp;

uint32_t* getCountArray(uint8_t* data, uint64_t size, uint64_t* symCount)
{
    static uint32_t buf[256] = {0};
    memset(buf, 0, 256*4); // обнуляем на случай повторного вызова функции compress

    //подсчитываем встречаемость символа, вместе с количеством РАЗНЫХ символов
    *symCount = 0;
    for( uint64_t i = 0; i < size; i++)
    {
        if (buf[data[i]] == 0) (*symCount)++;
        buf[data[i]]++;
    }
    return buf;
}

// удаление дерева Хаффмана
void deleteTree(struct Node* nd)
{
    if (nd->left)
    {
        deleteTree(nd->left);
        deleteTree(nd->right);
    }
    free(nd);
}

struct Node* getKeyTree(uint32_t *array, uint64_t symCount)
{
    //создаем массив листьев дерева, одновременно сортируя символы по их встречаемости
    struct Node ** a = (struct Node**)calloc(symCount,  sizeof(struct Node*));
    uint64_t j = 0;
    for(uint64_t i = 0; i < symCount; i++)
    {
        while (array[j] == 0) j++;
        struct Node *b = (struct Node*)malloc(sizeof(struct Node));
        b->parent = b->left = b->right = 0;
        b->value = j;
        b->count = array[j];
        uint64_t l = i;
        while ((l>0) && (a[l-1]->count < b->count))
        {
            a[l] = a[l - 1];
            l--;
        }
        a[l]= b;
        j++;
    }
    //далее "собираем" дерево Хаффмана, поочередно объединяя узлы с наименьшей встречамостью,
    //пока не останется один корневой узел
    for(uint64_t i = symCount; i > 1; i--)
    {
        struct Node *b = (struct Node*)malloc(sizeof(struct Node));
        b->parent = 0;
        b->left = a[i - 1];
        b->left->parent = b;
        b->right = a[i - 2];
        b->right->parent = b;
        b->value = 0;
        b->count =b->left->count + b->right->count;
#ifdef DEBUG1
        fprintf(fp, "%d = %d + %d \n", b->count, b->left->count, b->right->count);
#endif
        uint64_t l = i - 2;
        a[i - 1] = 0;
        //a[i - 2] = 0;
        while ((l>0) && (a[l-1]->count < b->count))
        {
            a[l] = a[l - 1];
            l--;
        }
        a[l]= b;
    }
    struct Node* buf = a[0];
    free(a);
    return buf;
};

//рекурсивно проходимся по дереву Хаффмана и создаем кодировку
void getKeyTable(struct Node* tree, struct Pair* table, uint8_t keysize, uint64_t k)
{
    //нет потомков - следовательно мы нашли конкретный символ
    if (tree->right == 0)
    {
        table[tree->value].keySize = keysize;
        table[tree->value].key = k;
#ifdef DEBUG
        fprintf(fp,"%c :%d,  %x \n", tree->value,keysize, k);
#endif // DEBUG
    }
    else
    {
        //закладываем 0 для левого потомка, 1 для правого
        getKeyTable(tree->left, table, keysize+1, (k << 1));
        getKeyTable(tree->right, table, keysize+1, (k << 1) + 1);
    }
}

//функция сжатия данных
void compress(uint8_t* dest, uint64_t *destL,
				uint8_t* src, uint64_t srcL)
{
    //кол-во различных символов
    uint64_t symCount = 0;
    //строим массив встречаемости символов
    uint32_t *qArray = getCountArray(src, srcL, &symCount);
    //строим дерево Хаффмана по массиву встречаемости
    struct Node* HTree = getKeyTree(qArray, symCount);
    //строим таблицу кодировки для каждого символа
    struct Pair table[256];
    getKeyTable(HTree, table, 0, 0);

#ifdef DEBUG
    fp = fopen("log.txt", "w");
    for( int i =0; i < 256; i++)
        if (qArray[i] != 0) fprintf(fp,"For %d symbol : %d \n", i, qArray[i]);
    //system("pause");

    //for( int i =0; i < 256; i++)
    //    fprintf(fp, "%d symbol:%d, 0x%08x \n", i, table[i].keySize, table[i].key);
    fclose(fp);
#endif // DEBUG

    //записываем кол-во сжимаемых символов и массив их встречаемости
    //это необходимо для распаковки в будущем
    *((uint64_t*)(dest)) = srcL;
    memcpy(dest+sizeof(uint64_t), qArray, 256 * sizeof(uint32_t));


    struct Pair* buffer = &table[src[0]]; //кодировка записывемого символа
    uint64_t codedSize = 0; //кол-во УЖЕ сжатых символов
    uint64_t pbyte =256 * sizeof(uint32_t) + sizeof(uint64_t); //читаемый байт
    uint64_t pbit = 0; //читаемый бит
    uint64_t ppair = 0; //номер бита из кодировки buffer

    while (1)
    {
        //проверка полностью ли мы закодировали символ
        if (ppair == buffer->keySize)
            if (++codedSize == srcL) break; //это последний
            else
            {
                //если не последний, переходим к следующему
                buffer = &table[src[codedSize]];
                ppair = 0;
            }
        //записываем бит из кодировки символа в нужное место
        dest[pbyte] |= (1 & (buffer->key >> (buffer->keySize - ppair - 1))) << (7 - pbit);

        //передвигаем все указатели
        ppair++;
        pbit++;
        if (pbit == 8)
        {
            pbit = 0;
            dest[++pbyte] = 0; //для правильной работы битовой операции
        }
    }
    //возвращаем объем сжатых данных и уничтожаем дерево Хаффмана
    *destL = pbyte;
    deleteTree(HTree);
}

//распаковка данных
void decompress(uint8_t* dest, uint64_t *destL,
				uint8_t* src, uint64_t srcL)
{
    uint64_t symCount = 0;
    //читаем кол-во закодированных символов
    *destL = *((uint64_t*)src);
    //устанавливаем указатель на место, где записан массив встречаемости символов,
    // который мы используем при распаковке
    uint32_t *qArray = src + sizeof(uint64_t);
    //считаем кол-во РАЗЛИЧНЫХ символов
    for( uint64_t i = 0; i < 256; i++)
        if (qArray[i] != 0) symCount++;
    //восстанавливаем кодировку для каждого символа
    struct Node* HTree = getKeyTree(qArray, symCount);

    //кол-во уже раскодированных символов
    uint64_t decodedp = 0;
    //мы читаем бит за битом проходясь от корня дерева Хаффмана до листьев, которые характеризуют конкретный символ
    struct Node* buffer = HTree;
    //проходим непрчоитанные байты
    for(uint64_t i=256 * sizeof(uint32_t) + sizeof(uint64_t); i< srcL; i++)
    {
        //и биты
        for(int j = 0; j < 8; j++)
        {
            //в зависимости от бита "спускаемся" по дереву Хаффмана
            if ((src[i] << j) & 0b10000000) buffer = buffer->right;
            else buffer = buffer->left;
            //если далее потомков нет - мы раскодировали символ
            if (!buffer->left)
            {
                dest[decodedp++] = buffer->value;
                buffer = HTree;
                if (decodedp == *destL) break;
            }
        }
    }
    deleteTree(HTree);
}

