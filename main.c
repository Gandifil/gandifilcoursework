#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "compressing.h"
#include "stats.h"

const uint32_t BUFFER_SIZE = (0b1 << 25); // =32 MBytes

//флаги, кторые обозначают аргументы комндной строки
const int8_t INFO_FLAG = 0b1;
const int8_t ENCODE_FLAG = 0b10;
const int8_t DECODE_FLAG = 0b100;
//функция, проверяющая наличие аргументов
int8_t readArguments(int argc, char *argv[])
{
    int8_t result = 0;
    for( int i = 0; i < argc; i++)
    {
        if (strcmp(argv[i], "-info") == 0) result |= INFO_FLAG;
        if (strcmp(argv[i], "-e") == 0) result |= ENCODE_FLAG;
        if (strcmp(argv[i], "-d") == 0) result |= DECODE_FLAG;
    }
    return result;
}

int main(int argc, char *argv[])
{
    uint8_t flags = readArguments(argc, argv);

    //читаем в буфер из stdin
    freopen(0, "rb", stdin);
    uint8_t* bufIn = (uint8_t* )malloc(BUFFER_SIZE*sizeof(uint8_t));
    size_t lenIn = fread(bufIn, sizeof(uint8_t), BUFFER_SIZE, stdin);

    //собираем статистику, если установлен флаг -info
    if ((flags & INFO_FLAG) != 0)
    {
        saveStatistics(bufIn, lenIn);
        return EXIT_SUCCESS;
    }

    //сжимаем или распаковываем в зависимости от наличия флагов
    uint8_t* bufOut = (uint8_t*)malloc(BUFFER_SIZE*sizeof(uint8_t));
    size_t lenOut;

    if ((flags & DECODE_FLAG) != 0)
        decompress(bufOut, &lenOut, bufIn, lenIn);
    else
        compress(bufOut, &lenOut, bufIn, lenIn);

    //записываем результат в stdout
    freopen(0, "wb", stdout);
    fwrite(bufOut, sizeof(uint8_t), lenOut, stdout);

    return EXIT_SUCCESS;
}
