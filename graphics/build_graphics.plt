set terminal jpeg size 1280, 1024

set output "ev_graphivs.jpg"
plot "ev_file.txt"using 1:2 with lines

set output "gd_graphivs.jpg"
plot "gd_file.txt"using 1:2 with lines

