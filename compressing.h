#include <stdio.h>
#include <inttypes.h>
#ifndef MY_COMPRESS
#define MY_COMPRESS
//#define DEBUG
//#define DEBUG1

void compress(uint8_t* dest, uint64_t *destL,
				uint8_t* src, uint64_t srcL);
void decompress(uint8_t* dest, uint64_t *destL,
				uint8_t* src, uint64_t srcL);

#endif
