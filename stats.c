#include "stats.h"

//файлы для сохранения данных статистики
const char EV_file[] = "graphics/ev_file.txt"; // expected value - мат. ожидание
const char GD_file[] = "graphics/gd_file.txt"; // general data

//график матожидания
void saveExpectedGraphic(uint8_t* bufIn, size_t lenIn)
{
    #define BLOCK_SIZE 10 * (1 << 10) //10 KByte
    //const int32_t block_size =
    const int32_t experiments_count = 1000;
    int8_t buffer[BLOCK_SIZE];
    int8_t freq[BLOCK_SIZE / 100] = {0};

    //инициализируем генератор случайных чисел
    time_t t;
    srand((unsigned) time(&t));

    //производим рассчеты
    for( int i = 0; i < experiments_count; i++)
    {
        uint64_t outsize = 0;
        // выбираем случайный набор данных
        uint8_t *inpt = bufIn + rand() % (lenIn - BLOCK_SIZE);
        compress(buffer, &outsize, inpt, BLOCK_SIZE);
        //запоминаем полученный результат
        freq[(outsize / 100) - 1]++;
    }

    //вывод
    FILE *fp = fopen(EV_file, "wr");
    for(int i = 0; i< (BLOCK_SIZE / 100); i++)
        fprintf(fp, "%d\t%f\n", (i+1)*100 , ((double)freq[i])/experiments_count);//
    close(fp);
}


void saveGeneralGraphic(uint8_t* bufIn, size_t lenIn)
{
    //const size_t interval = (1 << 20)*3; // =3 MBytes
    const int32_t block_size = 10 * (1 << 10); //10 KByte
    int8_t buffer[lenIn];

    FILE *fp = fopen(GD_file, "wr");
    //сжимаем и одновременно записываем результат, увеличивая данные с шагом в 10 КБайт
    for( size_t i = block_size; i <=lenIn; i+=block_size)
    {
        uint64_t outsize = 0;
        memset(buffer, 0, lenIn);
        compress(buffer, &outsize, bufIn, i);
        fprintf(fp, "%d\t%d\n", i, outsize);
    }
    close(fp);
}

void saveStatistics(uint8_t* bufIn, size_t lenIn)
{
    saveExpectedGraphic(bufIn, lenIn);
    saveGeneralGraphic(bufIn, lenIn);
}
